import time
start_time = time.time()

def is_composed_diff_number(number):
    list_number=[] 
    list_number[:0] = str(number) 
    if len(list(dict.fromkeys(list_number))) < len(list_number):
        return False
    else:
        return True

def is_there_a_0(number):
    list_number=[] 
    list_number[:0] = str(number) 
    if ('0' in list_number) == True:
        return True
    else:
        return False

def pandigital_sum_products():
    list_products = []
    for i in range (1,999):
        for j in range(9,9999):
            if (is_composed_diff_number(i) == False) or (is_there_a_0(i) == True):
                break
            if (((is_composed_diff_number(j) == True) and (is_composed_diff_number(i * j) == True)) and ((is_there_a_0(j) == False) and (is_there_a_0(i * j) == False))):
                if ((is_composed_diff_number(int(str(i)+str(j)+str(i * j))) == True ) and (len(str(i)+str(j)+str(i * j)) == 9)):
                    list_products.append(i * j)
    return sum(list(dict.fromkeys(list_products)))

print(pandigital_sum_products())
print(f"--- {(time.time() - start_time):.10f} seconds ---" )